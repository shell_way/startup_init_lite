/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "init_log.h"
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <time.h>
#include "securec.h"

#define UNUSED(x) \
    do { \
        (void)(x) \
    } while (0)

#define MAX_LOG_SIZE 1024
#define BASE_YEAR 1900
#define UNLIKELY(x)    __builtin_expect(!!(x), 0)

static InitLogLevel g_logLevel = INIT_INFO;
static const char *LOG_LEVEL_STR[] = { "DEBUG", "INFO", "WARNING", "ERROR", "FATAL" };

void SetInitLogLevel(InitLogLevel logLevel)
{
    g_logLevel = logLevel;
}

#ifdef OHOS_LITE
void InitToHiLog(InitLogLevel logLevel, const char *fmt, ...)
{
    if (logLevel < g_logLevel) {
        return;
    }

    va_list list;
    va_start(list, fmt);
    char tmpFmt[MAX_LOG_SIZE];
    if (vsnprintf_s(tmpFmt, MAX_LOG_SIZE, MAX_LOG_SIZE - 1, fmt, list) == -1) {
        va_end(list);
        return;
    }
    (void)HiLogPrint(LOG_CORE, logLevel, LOG_DOMAIN, INIT_LOG_TAG, "%{public}s", tmpFmt);
    va_end(list);
    return;
}
#endif

#ifndef INIT_AGENT // for init
static int g_fd = -1;
void OpenLogDevice(void)
{
    int fd = open("/dev/kmsg", O_WRONLY | O_CLOEXEC, S_IRUSR | S_IWUSR | S_IRGRP | S_IRGRP);
    if (fd >= 0) {
        g_fd = fd;
    }
    return;
}

void EnableDevKmsg(void)
{
    /* printk_devkmsg default value is ratelimit, We need to set "on" and remove the restrictions */
    int fd = open("/proc/sys/kernel/printk_devkmsg", O_WRONLY | O_CLOEXEC, S_IRUSR | S_IWUSR | S_IRGRP | S_IRGRP);
    if (fd < 0) {
        return;
    }
    char *kmsgStatus = "on";
    write(fd, kmsgStatus, strlen(kmsgStatus) + 1);
    close(fd);
    fd = -1;
    return;
}

void InitLog(InitLogLevel logLevel, const char *fileName, int line, const char *kLevel,
    const char *fmt, ...)
{
    if (logLevel < g_logLevel) {
        return;
    }

    if (UNLIKELY(g_fd < 0)) {
        OpenLogDevice();
        if (g_fd < 0) {
            return;
        }
    }
    va_list vargs;
    va_start(vargs, fmt);
    char tmpFmt[MAX_LOG_SIZE];
    if (vsnprintf_s(tmpFmt, MAX_LOG_SIZE, MAX_LOG_SIZE - 1, fmt, vargs) == -1) {
        close(g_fd);
        g_fd = -1;
        return;
    }

    char logInfo[MAX_LOG_SIZE];
    if (snprintf_s(logInfo, MAX_LOG_SIZE, MAX_LOG_SIZE - 1, "%s[pid=%d][%s:%d][%s][%s] %s",
        kLevel, getpid(), fileName, line, INIT_LOG_TAG, LOG_LEVEL_STR[logLevel], tmpFmt) == -1) {
        close(g_fd);
        g_fd = -1;
        return;
    }
    va_end(vargs);

    if (write(g_fd, logInfo, strlen(logInfo)) < 0) {
        close(g_fd);
        g_fd = -1;
    }
    return;
}
#else // for other process
static FILE *g_outfile = NULL;
void InitLog(InitLogLevel logLevel, const char *fileName, int line, const char *kLevel,
    const char *fmt, ...)
{
    if (logLevel < g_logLevel) {
        return;
    }
    time_t second = time(0);
    if (second < 0) {
        return;
    }
    struct tm *t = localtime(&second);
    if (t == NULL) {
        return;
    }

    if (g_outfile == NULL) {
        chmod(PARAM_AGENT_LOG_PATH, S_IRWXU | S_IRWXG | S_IRWXO);
        g_outfile = fopen(PARAM_AGENT_LOG_PATH, "w+");
    }
    if (g_outfile == NULL) {
        (void)fprintf(stdout, "%s[%d-%d-%d %d:%d:%d][pid=%d][%s:%d][%s][%s] ", kLevel, (t->tm_year + BASE_YEAR),
            (t->tm_mon + 1), t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec, gettid(), fileName,
            line, INIT_LOG_TAG, LOG_LEVEL_STR[logLevel]);
        printf("output %s error: %s \n", fmt, strerror(errno));
        return;
    }
    (void)fprintf(g_outfile, "%s[%d-%d-%d %d:%d:%d][pid=%d][%s:%d][%s][%s] ", kLevel,  (t->tm_year + BASE_YEAR),
        (t->tm_mon + 1), t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec, getpid(), fileName,
        line, INIT_LOG_TAG, LOG_LEVEL_STR[logLevel]);
    va_list list;
    va_start(list, fmt);
    (void)vfprintf(g_outfile, fmt, list);
    va_end(list);
    (void)fflush(g_outfile);
}
#endif